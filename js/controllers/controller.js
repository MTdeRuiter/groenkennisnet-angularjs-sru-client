/**
 * Created by mtderuiter on 12/2/13.
 */

var myApp = angular.module('SRUApp', ['ngResource']);

myApp.service('SRUService', ['$resource', function ($resource) {

  var baseURL = "http://zoekentest.groenkennisnet.nl/sru2/request";

  var defaults = {
    maximumRecords: 10,
    startRecord: 0,
    recordSchema: "data",
    callback: "JSON_CALLBACK",
    httpAccept: "application/javascript",
    facetLimit: "10,4:thema,4:keyword,4:collection,4:bron,4:digitalContent,4:,4:"
  };

  // add query as query param
  var SRUService =
    $resource(baseURL, defaults, {
      'get': {
        method: 'JSONP'
      }
    });

  return SRUService;

}]);

myApp.controller('listController', ['$scope', 'SRUService', function($scope, SRUService){

        $scope.getResponse = function() {
            SRUService.get({query:$scope.query}, function(data){

              console.log(data)
              $scope.results = data;

            });
        };

        $scope.query;

        $scope.results = "press a button"
    }]);

myApp.directive('facets', function() {
  return {
    // required to make it work as an element
    restrict: 'E',

    templateUrl: "/groenkennisnet/templates/facets.html",
    replace: true,
    // observe and manipulate the DOM
    link: function(scope, element, attrs) {
      console.log(scope);

      attrs.$observe('unit', function (value) {
        //
      })
    }
  }
})