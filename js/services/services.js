/**
 * Created by mtderuiter on 12/2/13.
 */

myApp.factory('query', ['$http',
    function($http){
        $http.jsonp('http://zoekentest.groenkennisnet.nl/sru2/request?callback=JSON_CALLBACK').
            succes(function(data, status, headers, config) {
                return(data)
            }).
            error(function(data, status, headers, config) {
                return(data)
            })
    }
])

